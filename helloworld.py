# https://github.com/noahgift/flask-change-microservice
# https://gist.github.com/ronilpatel/4b28e0f821ef119c8a8d223c89987f8a

from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    print("I am inside hello world")
    return 'Hello World!'

def test_hello():
    """Unit Test for hello funcition"""
    assert hello() == "Hello World!"

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
